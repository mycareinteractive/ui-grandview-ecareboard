/**
 * Careboard view module.
 */
var CareboardView = Ion.View.extend({

    template: 'careboard.html',
    css: 'careboard.css',
    id: 'careboard',


    render: function () {
        this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#date .header2 p')[0],
            format: 'dddd, mmm d - h:MM TT'
        }));
        var context = this;
        // schedule a timer every 60 seconds.
        $.doTimeout('careboard polling', 120000, function () {
            try {
                context._updateData();
            }
            catch (e) {
                Ion.error('Exception caught when updating data: ' + e);
            }
            return true;
        });
        // delay 1 second and do an one-time update
        $.doTimeout('careboard polling once', 500, function () {
            try {
                context._updateData();
            }
            catch (e) {
                Ion.error('Exception caught when updating data: ' + e);
            }
            return false;
        });

        return this;
    },

    uninit: function () {
        $.doTimeout('careboard polling');   // stop the timer
    },

    click: function ($obj) {
        Ion.log("** " + $obj.attr('id') + ' clicked **');
        return false;
    },

    _updateData: function () {

        var self = this;

        //get data from middleware
        var userData = Ion.fetcher.getUserData();

        var cnt = '';
        // Get Patient MRN
        var url = '', dataobj = {};
        var preferredname = '';
        var mrn = '';

        var x0 = Ion.fetcher.getMRN(true, true).done(function (ret) {
            if (ret && ret.DataArea && ret.DataArea.ListOfBill
                && ret.DataArea.ListOfBill[0] && ret.DataArea.ListOfBill[0].tagAttribute) {
                mrn = ret.DataArea.ListOfBill[0].tagAttribute.accountNumber;
            }
        });

        $.when(x0).done(function () {
            var main_url = window.portalConfig.getclinical;

            // Get Patient Preferred Name
            url = main_url + "type=patient&mrn=" + mrn + "&numrec=3&sortorder=asc";

            var dataobj = '';
            var x1 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    preferredname = ($(this).find("value").text());
                });
            });


            // Get Estimated Discharge Date
            url = main_url + "type=estimateddisdate&mrn=" + mrn + "&numrec=1&sortorder=asc";
            var estimateddisdate = '';
            var dataobj = '';
            var x2 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    var ed = ($(this).find("value").text());
                    if (ed != '') {
                        estimateddisdate = new Date(ed.substr(0, 4) + "/" + ed.substr(4, 2) + "/" + ed.substr(6, 2))
                        estimateddisdate = estimateddisdate.format("mm-dd-yyyy");
                    }
                });
            });

            var careteamcnt = 0;

            // Get Consults
            url = main_url + "type=consults&mrn=" + mrn + "&numrec=50&sortorder=asc";
            var dataobj = '';
            var enddate = '';
            var consults = '';
            cnt = 0;
            var x3 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    if (cnt <= 4) {
                        var desc = ($(this).find("codedescription").text());
                        var value = ($(this).find("value").text());
                        var enddate = ($(this).find("enddate").text());
                        var status = ($(this).find("orderStatus").text());
                        var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                        if (value != '    ') {
                            if (status != 'Completed' && status != 'Exam Completed') {
                                if (desc != '' && desc.length >= 1) {
                                    consults = consults + '<p>' + desc + ':<span class="text-color-1">' + value + '</span></p>';
                                } else {
                                    consults = consults + '<p>' + value + '</p>';
                                }
                                cnt = cnt + 1;
                                careteamcnt = careteamcnt + 1;
                            }
                        }
                    }
                });
            });


            // Get Careteam Docs Data
            url = main_url + "type=careteam-docs&mrn=" + mrn + "&numrec=1&sortorder=asc";
            var dataobj = '';
            var enddate = '';
            var careteamDocs = '';
            cnt = 0;
            var x4 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                //Ion.log(xml);
                $(xml).find("item").each(function () {
                    if (cnt <= 4) {
                        desc = ($(this).find("codedescription").text());
                        value = ($(this).find("value").text());
                        enddate = ($(this).find("enddate").text());
                        var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                        var datecompare = 0;
                        if (enddate.length >= 1)
                            datecompare = Ion.compareDates(enddate, now);
                        //Ion.log(datecompare + ' ' + value + ' ' + now + ' ' + enddate);
                        if (datecompare >= 0) {
                            if (desc != '' && desc.length >= 1) {
                                careteamDocs = careteamDocs + '<p>' + desc + ':<span class="text-color-1">' + value + '</span></p>';
                            } else {
                                careteamDocs = careteamDocs + '<p>' + value + '</p>';
                            }
                            cnt = cnt + 1;
                            careteamcnt = careteamcnt + 1;
                        }
                    }
                });
            });


            // Get Careteam Data
            url = main_url + "type=careteam&mrn=" + mrn + "&numrec=100&sortorder=asc";
            var dataobj = '';
            var enddate = '';
            var careteam = '';
            cnt = 0;
            var maxcareteam = 4 - careteamcnt;
            var x5 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                //Ion.log(xml);
                $(xml).find("item").each(function () {
                    if (cnt <= maxcareteam) {
                        desc = ($(this).find("codedescription").text());
                        // decided not to show relationship
                        //if (desc != 'Attending Physician')
                        //	desc = ''
                        value = ($(this).find("value").text());
                        enddate = ($(this).find("enddate").text());
                        var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                        var datecompare = 0;
                        if (enddate.length >= 1)
                            datecompare = Ion.compareDates(enddate, now);
                        //Ion.log(datecompare + ' ' + value + ' ' + now + ' ' + enddate);
                        if (datecompare >= 0) {
                            if (desc != '' && desc.length >= 1) {
                                careteam = careteam + '<p>' + desc + ':<span class="text-color-1">' + value + '</span></p>';
                            } else {
                                careteam = careteam + '<p>' + value + '</p>';
                            }
                            cnt = cnt + 1;
                        }
                    }
                });
            });


            // Get Diets
            url = main_url + "type=diet&mrn=" + mrn + "&numrec=250&sortorder=dsc";
            var dataobj = '';
            var dietdesc = '<p>';
            var checktest = new Array;
            var code = '';
            var x6 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    desc = ($(this).find("codedescription").text());
                    value = ($(this).find("value").text());
                    status = ($(this).find("orderStatus").text());
                    value = value.replace(/\;/g, '<br/>');
                    value = value.replace(/\:/g, '<br/>');
                    value = value.replace(/\,/g, '<br/>');
                    if (status != 'Completed' && status != 'Exam Completed') {
                        if (checktest[value] != value && value != ' ') {
                            checktest[value] = value;
                            dietdesc = dietdesc + value + '; ';
                        }
                    }
                });
                dietdesc = dietdesc + '</p>';
            });
            //Ion.log(dietdesc);

            // Get Activity
            url = main_url + "type=activity&mrn=" + mrn + "&numrec=6&sortorder=asc";
            var dataobj = '';
            var activity = '';
            var checktest = new Array;
            var code = '';
            var x7 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    desc = ($(this).find("codedescription").text());
                    value = ($(this).find("value").text());
                    code = ($(this).find("code").text());
                    status = ($(this).find("orderStatus").text());
                    value = value.replace(/\;/g, '<br/>');
                    value = value.replace(/\:/g, '<br/>');
                    value = value.replace(/\,/g, '<br/>');
                    if (status != 'Completed' && status != 'Exam Completed') {
                        if (checktest[value] != value && value != ' ') {
                            checktest[value] = value;
                            activity = activity + '<p>&#8226; ' + value + '</p>';
                        }
                    }
                    cnt = cnt + 1;
                });
            });

            // Get Tests
            url = main_url + "type=tests&mrn=" + mrn + "&numrec=1500&sortorder=asc";
            var dataobj = '';
            var tests = '';
            var checktest = new Array;
            var cnt = 0;
            var x8 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    if (cnt <= 10) {
                        desc = ($(this).find("codedescription").text());
                        value = ($(this).find("value").text());
                        status = ($(this).find("orderStatus").text());
                        value = value.replace(/\;/g, '<br/>');
                        value = value.replace(/\:/g, '<br/>');
                        value = value.replace(/\,/g, '<br/>');
                        if (status != 'Completed' && status != 'Exam Completed') {
                            if (checktest[value] != value && value != ' ') {
                                checktest[value] = value;
                                tests = tests + '<p>&#8226; ' + value + '</p>';
                                cnt = cnt + 1;
                            }
                        }
                    }
                });
            });


            //Get Icons
            url = main_url + "type=icon&mrn=" + mrn + "&numrec=2500&sortorder=asc";
            var dataobj = '';
            var icons = '<p>';
            var cnt = 0;
            var checkicons = new Array;
            var x9 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    var icon = '';
                    code = ($(this).find("code").text());
                    value = ($(this).find("value").text());
                    icon = '<div class="icon" id="' + value + '"></div>';
                    status = ($(this).find("orderStatus").text());
                    if (status != 'Completed' && status != 'Exam Completed') {
                        if (checkicons[icon] != icon && icon != '' && value != '') {
                            cnt = cnt + 1;

                            checkicons[icon] = icon;
                            icons = icons + icon;
                        }
                        if (cnt == 6)
                            icons = icons + '</p> <p>';
                    }


                });
                icons = icons + '</p>';
            });

            //Ion.log(icons);
            // Get Support Person
            var mysupport = '';
            url = main_url + "type=supportperson&mrn=" + mrn + "&numrec=1&sortorder=asc";
            var dataobj = '';
            var x10 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                var mysupportperson = '';
                //Ion.log('typof xml' + typeof xml.length);
                $(xml).find("item").each(function () {
                    mysupportperson = unescape($(this).find("value").text());
                });
                //Ion.log(mysupportperson);
                if (mysupportperson.length >= 1)
                    mysupport = mysupport + '<p>' + mysupportperson + '</p>'
            });

            $.when(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10).done(function () {
                if (preferredname) {
                    self.$('#basicinfo #name').text('"' + preferredname + '"');
                } else {
                    self.$('#basicinfo #name').text(userData.patient.userFullName);
                }
                self.$('#dischargedate').html('<div class="header2">Estimated Discharge:</div><p> ' + estimateddisdate + '</p>');

                careteam = careteamDocs + consults + careteam;
                var room = userData.home.roomNumber.toUpperCase();
                var basicinfo = "Room: " + room + '   |   Phone: ' + userData.home.phoneNumber + ""
                self.$('#basicinfo #roominfo').text(basicinfo);
                self.$('#careteam').html(careteam);
                self.$('#activity').html(activity);
                self.$('#tests').html(tests);
                self.$('#icons').html(icons);
                self.$('#diet').html(dietdesc);
                self.$('#supportperson').html(mysupport);
            });

        });
    }

});
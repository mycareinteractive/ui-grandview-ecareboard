/**
 * decline view module. 
 */
var DeclineView = Ion.View.extend({

    template: 'decline.html',
    css: 'decline.css',
    id: 'decline',
	
    render: function () {	
		var context = this;
		this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#date .header2 p')[0],
            format: 'dddd, mmmm d  h:MM TT'
        }));
		
		var userData = Ion.fetcher.getUserData();
		var room = userData.home.roomNumber.toUpperCase();
        var basicinfo = "Room: " + room + '   |   Phone: '  + userData.home.phoneNumber + ""
        this.$('#basicinfo #roominfo').text(basicinfo);        
        this.$('#basicinfo #name').html('&nbsp;');

    },
});
